package com.opensource.msgui.repo.user.provider.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.opensource.msgui.domain.user.User;

public interface UserMapper extends BaseMapper<User> {

}
