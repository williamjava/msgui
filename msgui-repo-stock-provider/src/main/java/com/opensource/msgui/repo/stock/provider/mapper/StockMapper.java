package com.opensource.msgui.repo.stock.provider.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.opensource.msgui.domain.stock.Stock;

public interface StockMapper extends BaseMapper<Stock> {

}
