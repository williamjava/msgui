package com.opensource.msgui.repo.stock.api;

import com.opensource.msgui.domain.stock.Stock;
import com.opensource.msgui.repo.base.service.api.BaseService;

public interface RepoStockService extends BaseService<Stock> {

}
