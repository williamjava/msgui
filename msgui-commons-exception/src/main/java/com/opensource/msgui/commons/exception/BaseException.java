package com.opensource.msgui.commons.exception;

/**
 * @author gui
 */
public class BaseException extends Exception {
  public BaseException() {}
  
  public BaseException(String msg) {
    super(msg);
  }
}
