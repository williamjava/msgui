package com.opensource.msgui.commons.exception;

/**
 * @author gui
 */
public class MsguiDAOException extends Exception {
  public MsguiDAOException() {}
  
  public MsguiDAOException(String msg) {
    super(msg);
  }
}
