package com.opensource.msgui.ctl.demo.controller.v1.service;

import com.opensource.msgui.ctl.demo.controller.v1.config.Sink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class PullMessageTask {
    @Autowired
    private Sink sink;

    @Scheduled(fixedRate = 5 * 1000)
    public void pullMessage() {
        sink.input5().poll((message) -> {
            String payload = (String) message.getPayload();
            System.out.println("pull msg: " + payload);
        }, new ParameterizedTypeReference<String>() {
        });
    }
}