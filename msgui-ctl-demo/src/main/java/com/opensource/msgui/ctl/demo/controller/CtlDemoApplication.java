package com.opensource.msgui.ctl.demo.controller;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication(scanBasePackages = {"com.opensource.msgui"})
@EnableDubbo
//@EnableBinding({Source.class, Sink.class})
//@EnableGuiAutoImport
public class CtlDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(CtlDemoApplication.class, args);

//        ConfigurableApplicationContext applicationContext = SpringApplication.run(CtlDemoApplication.class, args);
//        FirstClass firstClass = applicationContext.getBean(FirstClass.class);
//        firstClass.test();
    }
}