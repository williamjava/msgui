package com.opensource.msgui.ctl.demo.controller.v1.message.listener;

import org.springframework.stereotype.Component;

@Component
public class TestConsumer {
//    @StreamListener(value = Sink.INPUT)
    public void receive(String receiveMsg){
        System.out.println("TopicTest receive: " + receiveMsg + "，receiveTime = " + System.currentTimeMillis());
    }
}
