package com.opensource.msgui.ctl.demo.controller.v1.controller;

import com.opensource.msgui.ctl.demo.controller.v1.model.User;
import com.opensource.msgui.ctl.demo.controller.v1.service.SendService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;

@RestController
public class SendController {
//    @Autowired
//    private Source source;

//    @GetMapping("/send")
//    public String send(String msg){
//        MessageBuilder builder = MessageBuilder.withPayload(msg);
//        Message message = builder.build();
//        source.output().send(message);
//
//        return "Hello RocketMQ Binder, send " + msg;
//    }

    @Resource
    private SendService sendService;

    /**
     * 发送简单消息
     * @param msg
     * @return
     */
    @GetMapping("/send")
    public String send(String msg){
        sendService.send(msg);
        return "success";
    }

    /**
     * 发送简单消息 + 标签
     * @param msg
     * @return
     */
    @GetMapping("/sendWithTags")
    public String sendWithTags(String msg, String tag){
        sendService.sendWithTags(msg, tag);
        return "success";
    }

    /**
     * 发送对象消息 + 标签
     */
    @GetMapping("/sendObj")
    public String sendObj(String userName, String password, String tag){
        User user = new User(UUID.randomUUID().toString(), userName, password);
        sendService.sendObject(user, tag);
        return "success";
    }

    /**
     * 发送事务消息
     */
    @GetMapping("/sendTransactionMessage")
    public String sendTransactionMessage(String msg, int num){
        sendService.sendTransactionMessage(msg, num);
        return "success";
    }

    /**
     * 手动拉取消息
     */
    @GetMapping("/poll")
    public String poll(String msg){
        sendService.sendMassiveMessage(msg);
        return "success";
    }
}
