package com.opensource.msgui.ctl.demo.controller.v1.config;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding({MQSource.class, Sink.class})
public class MQConfig {

}
