package com.opensource.msgui.repo.order.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.opensource.msgui.domain.order.Order;

public interface OrderMapper extends BaseMapper<Order> {

}
