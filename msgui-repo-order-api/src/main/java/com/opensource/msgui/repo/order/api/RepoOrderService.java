package com.opensource.msgui.repo.order.api;

import com.opensource.msgui.domain.order.Order;
import com.opensource.msgui.repo.base.service.api.BaseService;

public interface RepoOrderService extends BaseService<Order> {

}