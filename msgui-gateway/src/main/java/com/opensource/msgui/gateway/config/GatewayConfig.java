package com.opensource.msgui.gateway.config;

import com.opensource.msgui.gateway.filter.FmsUrlFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    @Bean
    public FmsUrlFilter tokenFilter() {
        return new FmsUrlFilter();
    }
}
