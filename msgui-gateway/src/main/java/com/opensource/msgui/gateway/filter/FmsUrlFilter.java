package com.opensource.msgui.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author whj
 */
public class FmsUrlFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String pathPrefix = "/sso";
        System.out.println(exchange.getRequest().getHeaders().containsKey("Authorization"));
        System.out.println(exchange.getRequest().getHeaders().getFirst("Authorization"));
        /**
         * 如果不是以pathPrefix开头的URL请求，进行校验
         */
        if (!exchange.getRequest().getPath().value().startsWith(pathPrefix)) {
            //TODO 校验当前登录用户，使用有货代角色
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
            return exchange.getResponse().setComplete();
        }

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return -100;
    }
}