package com.opensource.msgui.busi.order.service.api;

import com.opensource.msgui.busi.base.service.api.BusiBaseService;
import com.opensource.msgui.domain.order.Order;

public interface OrderService extends BusiBaseService<Order> {
    void create(Order order);
}
