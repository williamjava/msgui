package com.opensource.msgui.repo.user.api;

import com.opensource.msgui.domain.user.User;
import com.opensource.msgui.repo.base.service.api.BaseService;

public interface RepoUserService extends BaseService<User> {

}
